# Install

1. Go to the setup page and choose **Game Systems**.
2. Click the **Install System** button, and paste in this manifest link:
    * [https://gitlab.com/cswendrowski/solarblade/raw/master/system.json](https://gitlab.com/cswendrowski/solarblade/raw/master/system.json)
3. Create a Game World using the Solarblade System

Compatible with FoundryVTT 0.3.x

# Description

